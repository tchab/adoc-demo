{ pkgs ? import <nixpkgs> {} }:

with pkgs;

stdenv.mkDerivation {
  name = "adoc-demo";
  buildInputs = [ asciidoctor ];
}
